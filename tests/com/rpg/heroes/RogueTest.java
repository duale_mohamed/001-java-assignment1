package com.rpg.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void testRogue_CheckInitialStats_AreCorrect() {
        // Arrange
        Heroes rogue = new Rogue("Baggins");
        int expectedLvl = rogue.level;
        int expectedStr = rogue.baseAttributes.strength;
        int expectedVIT = rogue.baseAttributes.vitality;
        int expectedDex = rogue.baseAttributes.dexterity;
        int expectedInt = rogue.baseAttributes.intelligence;
        // Act
        int actualLvl = rogue.getLevel();
        int actualStr = rogue.getBaseAttributes().strength;
        int actualVit = rogue.getBaseAttributes().vitality;
        int actualDex = rogue.getBaseAttributes().dexterity;
        int actualInt = rogue.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, actualLvl);
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);

    }
    @Test
    void testRogue_CheckLvl10Stats_AreCorrect() {
        // Arrange
        Heroes rogue = new Rogue("Trenbolone Acetate");
        rogue.lvlUpHero(9);
        int expectedLvl = rogue.level;
        int expectedStr = rogue.baseAttributes.strength;
        int expectedVIT = rogue.baseAttributes.vitality;
        int expectedDex = rogue.baseAttributes.dexterity;
        int expectedInt = rogue.baseAttributes.intelligence;
        // Act
        int actualLvl = rogue.getLevel();
        int actualStr = rogue.getBaseAttributes().strength;
        int actualVit = rogue.getBaseAttributes().vitality;
        int actualDex = rogue.getBaseAttributes().dexterity;
        int actualInt = rogue.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, 10); // just to show it works!
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);
    }

    @Test
    void testLvlUpRogue_LvlUpOnce_Success() {
        // Arrange
        Heroes rogue = new Rogue("Lord Voldemort");
        int expected = 2;
        rogue.lvlUpHero(1);
        // Act
        int actual = rogue.level;
        // Assert
        assertEquals(expected,actual);
    }
}