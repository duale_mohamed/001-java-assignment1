package com.rpg.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void testRanger_CheckInitialStats_AreCorrect() {
        // Arrange
        Heroes ranger = new Ranger("Baggins");
        int expectedLvl = ranger.level;
        int expectedStr = ranger.baseAttributes.strength;
        int expectedVIT = ranger.baseAttributes.vitality;
        int expectedDex = ranger.baseAttributes.dexterity;
        int expectedInt = ranger.baseAttributes.intelligence;
        // Act
        int actualLvl = ranger.getLevel();
        int actualStr = ranger.getBaseAttributes().strength;
        int actualVit = ranger.getBaseAttributes().vitality;
        int actualDex = ranger.getBaseAttributes().dexterity;
        int actualInt = ranger.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, actualLvl);
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);

    }
    @Test
    void testRanger_CheckLvl10Stats_AreCorrect() {
        // Arrange
        Heroes ranger = new Ranger("Trenbolone Acetate");
        ranger.lvlUpHero(9);
        int expectedLvl = ranger.level;
        int expectedStr = ranger.baseAttributes.strength;
        int expectedVIT = ranger.baseAttributes.vitality;
        int expectedDex = ranger.baseAttributes.dexterity;
        int expectedInt = ranger.baseAttributes.intelligence;
        // Act
        int actualLvl = ranger.getLevel();
        int actualStr = ranger.getBaseAttributes().strength;
        int actualVit = ranger.getBaseAttributes().vitality;
        int actualDex = ranger.getBaseAttributes().dexterity;
        int actualInt = ranger.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, 10); // just to show it works!
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);
    }

    @Test
    void testLvlUpRanger_LvlUpOnce_Success() {
        // Arrange
        Heroes ranger = new Ranger("Lord Voldemort");
        int expected = 2;
        ranger.lvlUpHero(1);
        // Act
        int actual = ranger.level;
        // Assert
        assertEquals(expected,actual);
    }

}