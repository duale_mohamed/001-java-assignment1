package com.rpg.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void testWarrior_CheckInitialStats_AreCorrect() {
        // Arrange
        Heroes warrior = new Warrior("Baggins");
        int expectedLvl = warrior.level;
        int expectedStr = warrior.baseAttributes.strength;
        int expectedVIT = warrior.baseAttributes.vitality;
        int expectedDex = warrior.baseAttributes.dexterity;
        int expectedInt = warrior.baseAttributes.intelligence;
        // Act
        int actualLvl = warrior.getLevel();
        int actualStr = warrior.getBaseAttributes().strength;
        int actualVit = warrior.getBaseAttributes().vitality;
        int actualDex = warrior.getBaseAttributes().dexterity;
        int actualInt = warrior.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, actualLvl);
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);

    }
    @Test
    void testWarrior_CheckLvl10Stats_AreCorrect() {
        // Arrange
        Heroes warrior = new Warrior("Trenbolone Acetate");
        warrior.lvlUpHero(9);
        int expectedLvl = warrior.level;
        int expectedStr = warrior.baseAttributes.strength;
        int expectedVIT = warrior.baseAttributes.vitality;
        int expectedDex = warrior.baseAttributes.dexterity;
        int expectedInt = warrior.baseAttributes.intelligence;
        // Act
        int actualLvl = warrior.getLevel();
        int actualStr = warrior.getBaseAttributes().strength;
        int actualVit = warrior.getBaseAttributes().vitality;
        int actualDex = warrior.getBaseAttributes().dexterity;
        int actualInt = warrior.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, 10); // just to show it works!
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);
    }

    @Test
    void testLvlUpWarrior_LvlUpOnce_Success() {
        // Arrange
        Heroes warrior = new Warrior("Lord Voldemort");
        int expected = 2;
        warrior.lvlUpHero(1);
        // Act
        int actual = warrior.level;
       // Assert
        assertEquals(expected,actual);
    }
}