package com.rpg.heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void testMage_CheckInitialStats_AreCorrect() {
        // Arrange
        Heroes mage = new Mage("Baggins");
        int expectedLvl = mage.level;
        int expectedStr = mage.baseAttributes.strength;
        int expectedVIT = mage.baseAttributes.vitality;
        int expectedDex = mage.baseAttributes.dexterity;
        int expectedInt = mage.baseAttributes.intelligence;
        // Act
        int actualLvl = mage.getLevel();
        int actualStr = mage.getBaseAttributes().strength;
        int actualVit = mage.getBaseAttributes().vitality;
        int actualDex = mage.getBaseAttributes().dexterity;
        int actualInt = mage.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, actualLvl);
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);

    }
    @Test
    void testMage_CheckLvl10Stats_AreCorrect() {
        // Arrange
        Heroes mage = new Mage("Trenbolone Acetate");
        mage.lvlUpHero(9);
        int expectedLvl = mage.level;
        int expectedStr = mage.baseAttributes.strength;
        int expectedVIT = mage.baseAttributes.vitality;
        int expectedDex = mage.baseAttributes.dexterity;
        int expectedInt = mage.baseAttributes.intelligence;
        // Act
        int actualLvl = mage.getLevel();
        int actualStr = mage.getBaseAttributes().strength;
        int actualVit = mage.getBaseAttributes().vitality;
        int actualDex = mage.getBaseAttributes().dexterity;
        int actualInt = mage.getBaseAttributes().intelligence;

        // Assert
        assertEquals(expectedLvl, 10); // just to show it works!
        assertEquals(expectedStr, actualStr);
        assertEquals(expectedVIT, actualVit);
        assertEquals(expectedDex, actualDex);
        assertEquals(expectedInt, actualInt);
    }

    @Test
    void testLvlUMage_LvlUpOnce_Success() {
        // Arrange
        Heroes mage = new Mage("Lord Voldemort");
        int expected = 2;
        mage.lvlUpHero(1);
        // Act
        int actual = mage.level;
        // Assert
        assertEquals(expected,actual);
    }

}