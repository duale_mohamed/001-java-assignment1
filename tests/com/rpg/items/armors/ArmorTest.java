package com.rpg.items.armors;

import com.rpg.attributes.PrimaryAttributes;
import com.rpg.items.enums.ArmorTypes;
import com.rpg.items.enums.EquipmentSlots;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    void weapon_UsedByWarrior_IsTrue() {
        // Arrange
        Armor testArmor = new Armor();
        // Act
        testArmor.setName("Common Plate Body Armour");
        testArmor.setLvlReq(1);
        testArmor.setEquipSlot(EquipmentSlots.BODY);
        testArmor.setArmor(ArmorTypes.PLATE);
        testArmor.setPrimaryAttributes(new PrimaryAttributes(2,2,0,0));
        // Takes too much time to make each "actual" variable when i can hard code it instead to save space and time.
        // Assert
        assertEquals(testArmor.getName(),"Common Plate Body Armour");
        assertEquals(testArmor.getLvlReq(),1);
        assertEquals(testArmor.getEquipSlot(),EquipmentSlots.BODY);
        assertEquals(testArmor.getArmor(),ArmorTypes.PLATE);
        //assertEquals(testArmor.getPrimaryAttributes(), ); // unsure of how to implement stats.


    }
    @Test
    void armor_UsedByWarrior_IsWrongType() {
        // Arrange
        Armor testArmor = new Armor();
        testArmor.setArmor(ArmorTypes.PLATE);
        // Act
        ArmorTypes actual = ArmorTypes.CLOTH; // should be some way to use exception here but unsure.
        // Assert
        assertEquals(testArmor.getArmor(),actual);
    }
}