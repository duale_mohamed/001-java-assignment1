package com.rpg.items.weapons;

import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.enums.WeaponTypes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    @Test
    void weapon_UsedByWarrior_IsTrue() {
        // Arrange
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLvlReq(1);
        testWeapon.setEquipSlot(EquipmentSlots.WEAPON);
        testWeapon.setWeaponType(WeaponTypes.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        // Act
        String actualName = "Common Axe";
        // Takes too much time to make each "actual" variable when i can hard code it instead to save space and time.
        // Assert
        assertEquals(testWeapon.getName(),actualName);
        assertEquals(testWeapon.getLvlReq(),1);
        assertEquals(testWeapon.getEquipSlot(),EquipmentSlots.WEAPON);
        assertEquals(testWeapon.getWeaponType(),WeaponTypes.AXE);
        assertEquals(testWeapon.getDamage(),7);
        assertEquals(testWeapon.getAttackSpeed(),1.1);
    }
    @Test
    void weapon_UsedByWarrior_IsWrongType() {
        // Arrange
        Weapon testWeapon = new Weapon();
        testWeapon.setWeaponType(WeaponTypes.BOW);
        // Act
        WeaponTypes actual = WeaponTypes.AXE; // should be some way to use exception here but unsure.
        // Assert
        assertEquals(testWeapon.getWeaponType(),actual);
    }

}