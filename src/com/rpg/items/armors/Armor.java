package com.rpg.items.armors;

import com.rpg.attributes.PrimaryAttributes;
import com.rpg.items.Item;
import com.rpg.items.enums.ArmorTypes;
import com.rpg.items.enums.EquipmentSlots;

public class Armor extends Item {
    // Fields
    ArmorTypes armor;
    PrimaryAttributes primaryAttributes = new PrimaryAttributes();

    // Constructor
    public Armor(String name, int lvlReq, EquipmentSlots equipSlot, ArmorTypes armor, PrimaryAttributes primaryAttributes) {
        super(name, lvlReq, equipSlot);
        this.armor = armor;
        this.primaryAttributes = primaryAttributes;
    }
    // Default constuctor
    public Armor() {

    }

    // GETTERS and SETTERS

    public ArmorTypes getArmor() {
        return armor;
    }

    public void setArmor(ArmorTypes armor) {
        this.armor = armor;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }
}
