package com.rpg.items.weapons;

import com.rpg.items.Item;
import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.enums.WeaponTypes;

public class Weapon extends Item {
    // Fields
    private WeaponTypes weaponType;
    private int damage;
    private double attackSpeed;
    private double dps;

    // Default constructor
    public Weapon() {}

    // Constructor with abstract attributes from Item class.
    public Weapon(String name, int lvlReq, EquipmentSlots equipSlot,
                  int damage, WeaponTypes weaponType, double attackSpeed, double dps) {
        super(name, lvlReq, equipSlot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.dps = dps;
    }

    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double getDps() {
        return dps;
    }

    public void setWeaponType(WeaponTypes weaponType) {
        this.weaponType = weaponType;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setDps(double dps) {
        this.dps = damage * attackSpeed;
    }

    public void axeWeapon () {
        name = "Common Axe";
        lvlReq = 5;
        damage = 10;
        attackSpeed = 1.1;
        dps = damage * attackSpeed;
    }
}
