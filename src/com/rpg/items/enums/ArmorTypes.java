package com.rpg.items.enums;

public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
