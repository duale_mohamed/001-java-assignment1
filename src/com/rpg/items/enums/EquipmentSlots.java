package com.rpg.items.enums;

public enum EquipmentSlots {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
