package com.rpg.items.enums;

public enum WeaponTypes {
        WAND,
        STAFF,
        DAGGER,
        SWORD,
        AXE,
        HAMMER,
        BOW

}
