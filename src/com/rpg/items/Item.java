package com.rpg.items;

import com.rpg.items.enums.EquipmentSlots;

public abstract class Item {
    // Fields
    protected String name;
    protected int lvlReq;
    protected EquipmentSlots equipSlot;

    // Default constuctor
    public Item (){}

    public Item(String name, int lvlReq, EquipmentSlots equipSlot) {
        this.name = name;
        this.lvlReq = lvlReq;
        this.equipSlot = equipSlot;
    }
    // GETTERS and SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvlReq() {
        return lvlReq;
    }

    public void setLvlReq(int lvlReq) {
        this.lvlReq = lvlReq;
    }

    public EquipmentSlots getEquipSlot() {
        return equipSlot;
    }

    public void setEquipSlot(EquipmentSlots equipSlot) {
        this.equipSlot = equipSlot;
    }

    protected Item(String name) {
    }


}
