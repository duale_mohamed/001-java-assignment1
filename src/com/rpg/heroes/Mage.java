package com.rpg.heroes;

import com.rpg.exceptions.WrongArmorException;
import com.rpg.exceptions.WrongWeaponException;
import com.rpg.items.armors.Armor;
import com.rpg.items.enums.ArmorTypes;
import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.enums.WeaponTypes;
import com.rpg.items.weapons.Weapon;

public class Mage extends Heroes{

    // default constuctor
    public Mage(String name) {
        super(name, 5, 1, 1, 8);
    }

    public void lvlUpHero (int lvlUp) {
        level += 1 * lvlUp;
        baseAttributes.vitality += 3 * lvlUp;
        baseAttributes.strength += 1 * lvlUp;
        baseAttributes.dexterity += 1 * lvlUp;
        baseAttributes.intelligence += 5 * lvlUp;
    }

    // Exception Weapon
    public void weaponEquiped (Weapon wep) throws WrongWeaponException {
        if (wep.getLvlReq() > this.level) {
            throw new WrongWeaponException(wep, this.getClass());
        }
        if(wep.getWeaponType() == WeaponTypes.WAND || wep.getWeaponType() == WeaponTypes.STAFF) {
            equip.put(EquipmentSlots.WEAPON, wep);
            System.out.println(equip);
        }
        else {
            throw new WrongWeaponException(wep, this.getClass());
        }
    }

    // Exception Armor
    public void armorEquiped (Armor arm) throws WrongArmorException {
        if (arm.getLvlReq() > this.level) {
            throw new WrongArmorException(arm, this.getClass());
        }
        if(arm.getArmor() == ArmorTypes.CLOTH) {
            equip.put(EquipmentSlots.HEAD, arm);
            System.out.println(equip);
        }
        else {
            throw new WrongArmorException(arm, this.getClass());
        }
    }

}
