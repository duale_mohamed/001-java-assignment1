package com.rpg.heroes;

import com.rpg.attributes.PrimaryAttributes;
import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.Item;

import java.util.HashMap;

public abstract class Heroes {
    protected String name;
    protected int level;
    protected PrimaryAttributes baseAttributes = new PrimaryAttributes();
    protected PrimaryAttributes totalAttributes = new PrimaryAttributes();
    protected int charDPS;

    // Default constuctor
    Heroes() {}

    // Constructor
    public Heroes(String name, int vitality, int strength, int dexterity, int intelligence) {
        this.name = name;
        this.level = 1;
        this.baseAttributes.vitality = vitality;
        this.baseAttributes.strength = strength;
        this.baseAttributes.dexterity = dexterity;
        this.baseAttributes.intelligence = intelligence;
    }


    // abstact implent
    public abstract void lvlUpHero(int lvlUp);

    // Equipment HashMap
    HashMap<EquipmentSlots, Item> equip = new HashMap<EquipmentSlots, Item>();

    // HashMap Getter
    public HashMap<EquipmentSlots, Item> getEquip() {
        return equip;
    }

    // GETTERS
    public int getLevel() {
        return level;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }


    // toString Method
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Heroes{");
        sb.append("name='").append(name).append('\'');
        sb.append(", level=").append(level);
        sb.append(", baseAttributes=").append(baseAttributes);
        sb.append(", totalAttributes=").append(totalAttributes);
        sb.append(", charDPS=").append(charDPS);
        sb.append('}');
        return sb.toString();
    }



}
