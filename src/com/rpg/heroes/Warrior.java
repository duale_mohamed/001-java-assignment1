package com.rpg.heroes;

import com.rpg.exceptions.WrongArmorException;
import com.rpg.exceptions.WrongWeaponException;
import com.rpg.items.armors.Armor;
import com.rpg.items.enums.ArmorTypes;
import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.enums.WeaponTypes;
import com.rpg.items.weapons.Weapon;
import com.rpg.attributes.PrimaryAttributes;

public class Warrior extends Heroes{

    // default constuctor
    public Warrior(String name) {
        super(name, 5, 2, 1, 10);
    }

    public void lvlUpHero (int lvlUp) {
        level += 1 * lvlUp;
        baseAttributes.vitality += 5 * lvlUp;
        baseAttributes.strength += 3 * lvlUp;
        baseAttributes.dexterity += 2 * lvlUp;
        baseAttributes.intelligence += 1 * lvlUp;
    }

    // Exception Weapon
    public void weaponEquiped (Weapon wep) throws WrongWeaponException {
        if (wep.getLvlReq() > this.level) {
            throw new WrongWeaponException(wep, this.getClass());
        }
        if(wep.getWeaponType() == WeaponTypes.SWORD || wep.getWeaponType() == WeaponTypes.AXE || wep.getWeaponType() == WeaponTypes.HAMMER) {
            equip.put(EquipmentSlots.WEAPON, wep);
            System.out.println(equip);
        }
        else {
            throw new WrongWeaponException(wep, this.getClass());
        }
    }

    // Exception Armor
    public void armorEquiped (Armor arm) throws WrongArmorException {
        if (arm.getLvlReq() > this.level) {
            throw new WrongArmorException(arm, this.getClass());
        }
        if(arm.getArmor() == ArmorTypes.MAIL || arm.getArmor() == ArmorTypes.PLATE) {
            equip.put(EquipmentSlots.HEAD, arm);
            System.out.println(equip);
        }
        else {
            throw new WrongArmorException(arm, this.getClass());
        }
    }
}
