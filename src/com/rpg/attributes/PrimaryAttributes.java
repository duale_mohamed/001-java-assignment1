package com.rpg.attributes;

// Custom Type parameter
public class PrimaryAttributes {
    public int vitality;
    public int strength;
    public int dexterity;
    public int intelligence;

    public PrimaryAttributes(){
        this.vitality = 0;
        this.strength = 0;
        this.dexterity = 0;
        this.intelligence = 0;
    }

    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }
}