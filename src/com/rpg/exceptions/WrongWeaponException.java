package com.rpg.exceptions;

import com.rpg.items.weapons.Weapon;

public class WrongWeaponException extends Exception{
    public WrongWeaponException(Weapon wep, Class T) {
        super(String.format("Your character CANNOT Equip Weapon because of either LVL or Class restrictions!", T, wep.getWeaponType()));
    }
}
