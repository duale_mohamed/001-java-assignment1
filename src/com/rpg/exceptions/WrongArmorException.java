package com.rpg.exceptions;

import com.rpg.items.armors.Armor;

public class WrongArmorException extends Exception {
    public WrongArmorException (Armor armor, Class T) {
        super(String.format("Your character CANNOT Equip Weapon because of either LVL or Class restrictions!", T, armor.getClass()));
    }
}
