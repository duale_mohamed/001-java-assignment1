package com.rpg;

import com.rpg.exceptions.WrongWeaponException;
import com.rpg.heroes.Mage;
import com.rpg.heroes.Ranger;
import com.rpg.heroes.Warrior;
import com.rpg.items.enums.EquipmentSlots;
import com.rpg.items.enums.WeaponTypes;
import com.rpg.items.weapons.Weapon;


public class Main {

    public static void main(String[] args) {
        // Warrior
        Warrior warrior = new Warrior("Bighead");
        warrior.lvlUpHero(2);
        warrior.lvlUpHero(2);
        // Warrior weapon
        Weapon bigAxe = new Weapon("big Axe",1, EquipmentSlots.WEAPON, 10, WeaponTypes.AXE,1.2,12);
        // Try Catch
        try {
            warrior.weaponEquiped(bigAxe);
        } catch (WrongWeaponException e) {
            e.printStackTrace();
        }
        System.out.println(warrior);

        // Mage
        Mage mage = new Mage("dickhead");
        mage.lvlUpHero(2);
        // Mage weapon
        Weapon bigStaff = new Weapon("Very Big Staff",1,EquipmentSlots.WEAPON,12,WeaponTypes.STAFF,0.9,10);
        try {
            mage.weaponEquiped(bigStaff);
        } catch (WrongWeaponException e) {
            e.printStackTrace();
        }
        System.out.println(mage);

        // Ranger
        Ranger ranger = new Ranger("Halo");
        ranger.lvlUpHero(2); // lvl up twice
        System.out.println(ranger);
    }
}
